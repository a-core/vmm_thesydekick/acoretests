# ACoreTests

This entity contains:
1. Test configurations for the A-Core processor, and
2. A command line interface for executing those tests

## Quickstart
Run `./configure && make help` for help on running tests on command line.

## General Principles

- Three core functionalities:
    1. Generate Verilog description of A-Core processors from chisel source code. A-Core configuration is determined by hw-config files under `tests/hw-configs`. See e.g. `make chisel` and `make <hw-config>` recipes.
    2. Compile software targeted for a specific A-Core configuration. Software configuration is determined by test-config files under `tests/test-configs`. See e.g. `make sw` and `make <test-config>` recipes.
    3. Run a testbench (simulation or FPGA). Runs the first two steps automatically if not yet done. See `make sim` recipe.
- All generated files are placed under appropriate directories under `build/`. Note that files under that folder are not version-controlled.
- Some bits are dependency checked, some are not:
    - Chisel builds are target-checked, meaning if a hw-config is found under `build/chisel/<hw-config>`, the same config won't be compiled for a test that depends on the config. If you make modifications to chisel source code, you must run `make clean-chisel` or `make clean-<hw-config>` before running again.
    - Software works similarly, so if you want to recompile code, you should run `make clean-sw` or `make clean-<test-config>` before running again.

## Common errors
```
fatal error: a-core.h: No such file or directory
```
- You probably haven't compiled chisel for the requested hw-config
```
mv: cannot stat 'build/tests/programs/xx/xx/xx.elf': No such file or directory
```
- Make sure your software program generates an elf called `<platform>.elf`, i.e. `sim.elf` for simulations and `fpga.elf` for fpga builds. This error can also be caused by any other compilation-related problem.
```
fatal error: no input files
```
- Make sure submodules have been initialized
```
make[1]: riscv64-unknown-elf-gcc: Command not found
```
RISC-V GNU toolchain not in path. On ECD cluster machines, navigate to `a-core_thesydekick` and run `source sourceme.csh`. Check [here](https://gitlab.com/a-core/a-core_documentation/tutorials/-/blob/master/getting_started.md?ref_type=heads#122-local-machine) for instructions on installing the toolchain locally.
### YAML-Based Configuration
- Test configuration sources stored as YAML files under `tests/`
- Test configuration contains template variables for local paths, which are resolved by executing `./configure`. The
fully resolved configuration files are generated under `build/tests`
- Currently defined template variables
    - `{{ENTITY_PATH}}` -- to be replaced with absolute path to `ACoreTests`
    - `{{ENTITY_BUILD_PATH}}` -- to be replaced with `{{ENTITY_PATH}}/build`
    - `{{TEST_CONFIG_PATH}}` -- this can be used in an *extra test config* to be replaced with the absolute path of the provided test config
### Separation of Concern between Build System and Test Execution
- Build system
    - Good at tracking source dependencies
    - Should be configured to generate a nice set of executable test artefacts
        - generate test-specific:
            - hardware netlists -- `build/chisel/<hw-config>/*.v`
            - hardware specific software headers -- `build/chisel/<hw-config>/include/*.h`
            - test binaries executed by the simulated processor `build/tests/programs/<test-config>/<hw-config>/bin/<platform>.elf`
- Test execution
    - testbenches execute different test configurations on the corresponding test platform using the input files generated by the build system.

## Terminology

### Test Configuration

A _Test Configuration_ is a set of:
- hardware configuration
- compiled executable RISC-V program
- platform-specific configuration (simulator / fpga toolchain)

Test configurations are stored under `tests/test-configs/` as YAML files.

A test configuration defines the input files required by the downstream test platform.

Any time a modification is made on a test configuration, or when new ones are added, `configure` must be run again.

### Hardware Configuration

A _Hardware Configuration_ is a set of arguments passed to a chisel generator.
Hardware configurations are stored under `tests/hw-configs/core` as YAML files. 
JTAG configuration files are stored under `tests/hw-configs/jtag`.

For example we could have the following configuration:

```shell
$ cat tests/hw-configs/core/rv32imf_16k_16k.yml
```
```yaml
isa_string: rv32imf
data_width: 32
addr_width: 32
progmem_depth: 14 # 16 kB
datamem_depth: 14 # 16 kB
bus_type: "AXI4L"
pc_init: "h1000"
dCache:
  tag_memory_type: "ASYNC"
  cache_memory_type: "SYNC"
  depth: 128
  line_depth: 4
  option: "ALWAYS_ON"
  write_policy: "WRITE_THROUGH"
debug: false
jtag_config_yaml: {{ENTITY_PATH}}/tests/hw-configs/jtag/jtag-config.yml
```

See ACoreChip `README.md` for explanations on all configuration options.

Before running tests, the Chisel generator is run against required hardware
configurations and the build artefacts are generated under `build/chisel/<hw-config>/`.

For example, when executing a test requiring `rv32imf_16k_16k.yml` hardware
configuration, it is first built under `build/chisel/rv32imf_16k_16k/`. Any
test using this hardware configuration can then share the build outputs meaning
that we only need to run the generator once.

### Test Program

A _test program_ is a test configuration specific RISC-V executable.

Tests programs can be written either in assembly or C.
- Assembly tests
    - Located under `sw/riscv-assembly-tests`. The template `riscv-assembly-template` is linked as a Git submodule. It includes the base structure for an assembly program, including linker script and Makefile. When creating a new test, copy this folder, rename the copy, and remove its `.git` file.
- C tests
    - Located under `sw/riscv-c-tests`. The template `riscv-c-template` is linked as a Git submodule. It includes the base structure for a C-program, including linker script, startup script, and Makefile. When creating a new test, copy this folder, rename the copy, and remove its `.git` file.

Test program compilation depends on:
1. Version controlled test sources, and
2. Hardware-specific generated headers

In general, a test program sources can be compiled against multiple test configurations
which might have different hardware configurations (e.g. memory map). Therefore, the
test program needs to be compiled separately for each test configuration.

The software executable of a test configuration is compiled to
`build/tests/<test-config>/<hw-config>/bin/<platform>.elf` where `<test-config>` is the name of
the target test configuration, `<hw-config>` is the name of hardware configuration, and `<platform>` is the test execution platform (`sim` or `fpga`).

### Test Configuration

For example we could have the following configuration:

```shell
$ cat tests/test-configs/hello-world.yml
```
```yaml
hw_config_yaml: {{ENTITY_BUILD_PATH}}/tests/hw-configs/core/rv32imc.yml
test_program: {{ENTITY_PATH}}/sw/riscv-c-tests/src/hello_world

sim:
  selfchecking: false
  timeout: 50000
make_args:
    ARG: arg_val
```

- `hw_config_yaml` - path to the hw_config that the test uses. This defines which extensions the program is compiled with, memory sizes and addresses, for example
- `test_program` - path to the folder where your test program is located
- `sim` - simulation related options for this program
    - `selfchecking` - whether the program uses functions defined in `a-core-library` to check whether a test succeeds or not
    - `timeout` - timeout for the simulation in number of clock cycles. This is used for both selfchecking and non-selfchecking programs. Not necessary to provide, but the default value is very large.
- `make_args` - this option allows you to propagate arbitrary key-value pairs to the make command. In this example, the make command would result in `make (default arguments) ARG=arg_val`

## Development

### Adding new arguments
If your test or a testbench requires new arguments, they can be added by:

1. Add the new argument to `argparse` configuration by creating a new `parser.add_argument` in `ACoreTests` entity under `ACoreTest/__init__.py`
2. Add the new argument in `configure` both under `# LIST OF ARGUMENTS` and to the `make sim` recipe`

In `ACoreTests` entity, the new parser argument is automatically added to the set of keyword arguments (`**kwargs`) that is sent to both the test invocation method and the testbench class

### Adding new tests

Adding a new test to the flow is very simple. Say we want to add a test called `final-test`.
    1. Create a new test-config under `tests/test-configs/final-test.yml`. Use an old one as template. Make `hw_config_yaml` point to the configuration of your choice. Make `test_program` point to the program source folder.
    2. Re-run `configure`.
    3. You can now run the test with `make sim test_config=final-test`.

## riscv-tests
A set of tests are provided by the official [riscv-tests](https://github.com/riscv-software-src/riscv-tests). The config file is `riscv-tests.yml`. Notice the argument `make_args: RISCV_TEST_NAME`: this value selects which test is run. The test name is composed as follows (let's use `rv32ui-p-add` as an example):
- `rv32` is constant for our 32-bit RISC-V processor
- `ui` is the test set. You can observe all test sets under `sw/riscv-tests/riscv-tests/isa`
- `p` is the type of target environment, see [riscv-tests README](https://github.com/riscv-software-src/riscv-tests#readme) for more details. Do not change this.
- `add` is the single test within the test set. This name points to the `.S` file under `sw/riscv-tests/riscv-tests/isa/rv32ui` which holds the actual test code

There is also a script `scripts/run-riscv-tests.py` which is run when the user calls `make run-riscv-tests`. This script runs all tests for a certain test set sequentially. For simulations, it also composes results and prints them. To run all tests for `ui` test set, you would run:
```
make run-riscv-tests riscv_test_set=ui
```
All other arguments, like `simulator`, `sim_backend`, and `platform`, get propagated as well. Fastest combo is probably `simulator=cocotb sim_backend=icarus` for simulation runs thanks to icarus's fast startup time.

## Testing with Spike ISA simulator
There is rudimentary support for comparing register values against Spike ISA simulator. Essentially, the same elf file is given both to A-Core and Spike, and after each instruction, the register contents are compared. The register values are saved to a logfile `sim.log` located under `Entities/ACoreTestbenches/ACoreTestbenches`. If there is a mismatch, the logging stops and the last row is the mismatching register.

The simulation is enabled by selecting `simulator=cocotb spike=true`. You can also save the waveform file and set simulator backend like normal.

You need `py-spike`. It is located [here](https://gitlab.com/a-core/py-spike). Install it with `python3 -m pip install -e --user .`.

There are quite a few limitations, for example:
- You cannot use peripherals, i.e. UART or GPIO, because the Spike simulator does not have them. So, do not use prints or initialize UART. Same goes for custom CSRs (although you can write to `mstopsim` as the last simulation command to trigger simulation stop).
- Simulation time increases quite a bit thanks to invoking Spike every clock cycle
- The outputs are a bit messy, e.g. terminal does not properly show you whether test succeeded or failed. See `sim.log` for the test result.

If there is a mismatch in register values between spike and A-Core, *the test does not end immediately*. The user has a choice of continuing the test by pressing enter on the terminal. However, the terminal does not tell you whether such an event has occured - the message is printed in the logfile. This feature is useful for, for example, programs that use `clock()`, because the returned value is different for A-Core and spike, but it usually does not affect the program in other ways.


## API Documentation
- Python documentation for [ACoreTests](https://a-core.gitlab.io/a-core_thesydekick/ACoreTests/)
