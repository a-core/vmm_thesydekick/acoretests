import os
import yaml

def load_test_config_yaml(filepath):
    """
    Recursively load test configuration from yaml configuration files.
    Configuration file paths are relative to ``entitypath``.

    Relative paths to configuration files are replaced with corresponding
    absolute paths so that the loaded configurations can be easily used in
    other entities that don't necessarily know ACoreTests entitypath.

    Keys ending with ``_yaml`` contain paths to yaml configuration files.
    The contents of these files are loaded to keys without the suffix.

    For example, the contents of ``hw_config_yaml`` are loaded into dictionary
    corresponding to key ``hw_config``.
    """

    loader = yaml.Loader
    test_config = None

    with open(filepath, 'r') as fd:
        test_config = yaml.load(fd, loader)

        # TODO: implement recursive loader using the _yaml suffix convention that
        #       iterates until all yaml files have been loaded in the hierarchy.

        # load hw_config
        test_config['hw_config'] = load_hw_config_yaml(test_config['hw_config_yaml'])

    return test_config

def load_hw_config_yaml(filepath):
    loader = yaml.Loader
    # load hw_config
    with open(filepath, 'r') as fd:
        hw_config = yaml.load(fd, loader)

    # load hw_config.jtag_config
    with open(hw_config['jtag_config_yaml'], 'r') as fd:
        hw_config['jtag_config'] = yaml.load(fd, loader)
    return hw_config
