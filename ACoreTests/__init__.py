"""
ACoreTests
==========

This entity contains test configurations for test benches defined in YAML format.
The ACoreTests entity acts as an CLI interface for executing these test configurations.

"""

import os
import sys
import argparse
if not (os.path.abspath('../../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../../thesdk'))

import glob
import numpy as np
import multiprocessing as mp
import shutil

from thesdk import *
from acore_methods import *
import acore_methods.pathutils as pathutils
from ACoreTestbenches import *
from ACoreTests.test_config import load_test_config_yaml

class ACoreTests(thesdk):

#region properties

    @property
    def platform(self):
        """
        Execution platform.

        Options: ``'sim' | 'fpga'``.
        """
        if not hasattr(self, '_platform'):
            self._platform = None
        return self._platform
    @platform.setter
    def platform(self, value):
        if value in ['sim', 'fpga']:
            self._platform = value
        else:
            raise ValueError("invalid value: %s" % (value))

    @property
    def n_concurrent(self):
        """
        Maximum number of parallel runners for riscv-tests.
        """
        if not hasattr(self, '_n_concurrent'):
            self._n_concurrent = None
        return self._n_concurrent
    @n_concurrent.setter
    def n_concurrent(self, value):
        self._n_concurrent = value

    @property
    def riscv_test(self):
        """
        TODO: document this property
        """
        if not hasattr(self, '_riscv_test'):
            self._riscv_test = None
        return self._riscv_test
    @riscv_test.setter
    def riscv_test(self, value):
        self._riscv_test = value

#endregion properties

    def __init__(self, **kwargs):
        self.print_log(type='I', msg='Inititalizing %s' %(__name__))
        self.model = 'py'
        self.init(**kwargs)

    def init(self, **kwargs):
        self.testbench = None
        acore_methods.init_props_from_kwargs(self, **kwargs)

    def _init_testbench(self, **kwargs):
        if kwargs['platform'] == "sim":
            tb = SimTestbench(**kwargs)
            return tb
        elif kwargs['platform'] == "fpga":
            if kwargs['board'] == 'zc706':
                tb = ZC706Testbench(**kwargs)
                return tb
            else:
                self.print_log(type='F', msg="Unsupported FPGA board: %s" % (platform))
        else:
            self.print_log(type='F', msg="Undefined platform: %s" % (platform))


    def run(self, **kwargs):
        if self.model=='py':

            test_target = kwargs['test_target']

            test_config_filepath = os.path.join(self.entitypath, 'build/tests/test-configs/%s.yml' % test_target)
            self.print_log(type='I', msg=test_config_filepath)

            # look up and load yaml config of the given test_target
            test_config = None
            try:
                test_config = load_test_config_yaml(test_config_filepath)
            except FileNotFoundError:
                self.print_log(type='F', msg="The requested test configuration doesn't exist: %s" % test_config_filepath)

            kwargs.update({'test_config': test_config})

            # run generic sim testbench
            self.testbench = self._init_testbench(**kwargs)

            # paths for processing access
            test_dir = os.path.join(
                self.entitypath,
                'build',
                'tests',
                'programs',
                self.testbench.test_target,
                self.testbench._hw_config_name
            )

            sw_build_dir = os.path.join(test_dir, 'sw-build')
            sys.path.append(os.path.join(sw_build_dir, 'scripts'))

            results_dir = os.path.join(test_dir, 'results')
            if not os.path.exists(results_dir):
                os.mkdir(results_dir)

            sim_out = os.path.join(results_dir, 'sim_out')

            processDict = {} #To move data between pre and post scripts

            # Preprocess
            if kwargs['preprocess']:
                self.print_log(type='I', msg="Start pre-processing")

                self.testbench.dut.preserve_iofiles = True

                import preprocess
                preP = preprocess.preprocess(processDict)
                preP.run(results_dir)

                self.print_log(type='I', msg="Pre-processing finished!")

            # lookup test configuration method by name
            if kwargs['profile']:
                profile(self.testbench.run)
            else:
                self.testbench.run()

            # Postprocess
            if kwargs['postprocess']:
                self.print_log(type='I', msg="Start post-processing")

                import postprocess
                postP = postprocess.postprocess(processDict)
                postP.run(results_dir=results_dir, sim_out=sim_out)


                self.print_log(type='I', msg="Post-processing finished!")


# helper for profiling execution time taken by function calls
def profile(func, **kwargs):
    import cProfile, pstats
    profiler = cProfile.Profile()
    profiler.enable()
    func(**kwargs)
    profiler.disable()
    stats = pstats.Stats(profiler).sort_stats('cumtime')
    stats.print_stats()

# helper for parsing boolean values
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

if __name__=="__main__":
    from  ACoreTests import *
    import matplotlib.pyplot as plt
    from acore_methods import *

    # Parse tests to be executed from command line arguments
    # Remember to add the arguments also in Makefile (in configure)!
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "test_target",
        help="Test configuration to run.",
    )
    parser.add_argument(
        "--platform",
        help="Testbench target platform.",
        choices=[
            "sim",
            "fpga"
        ]
    )
    parser.add_argument(
        "--board",
        help="FPGA board type.",
        choices=['zc706']
    )
    parser.add_argument(
        "--simulator",
        help="Simulator to use",
        choices=[
            "thesdk",
            "cocotb"
        ]
    )
    parser.add_argument(
        "--questa_wave_fmt",
        help="Output waveform format for Questa Simulator",
        choices=[
            "wlf",
            "vcd",
            "none"
        ]
    )
    parser.add_argument(
        "--cocotb_wave_fmt",
        help="Output waveform format for cocotb flow",
        choices=[
            "vcd",
            "fst"
        ]
    )
    parser.add_argument(
        "--n_concurrent",
        help="Max number of concurrent tests",
        type=int
    )
    parser.add_argument(
        "--riscv_test",
        help="Specific riscv-test to run",
    )
    parser.add_argument(
        "--sim_backend",
        help="Simulator backend",
        choices=[
            "questa",
            "icarus",
            "verilator"
        ]
    )
    parser.add_argument(
        "--custom_top",
        help="Define a custom top level Verilog module",
        type=str
    )
    parser.add_argument(
        "--spike",
        help="Run simulation in parallel with spike, comparing outputs",
        type=str2bool
    )
    parser.add_argument(
        "--interactive_sim",
        help="Toggle interactive simulation off with False",
        type=str2bool
    )
    parser.add_argument(
        "--extra_sources",
        help="Extra Verilog sources for the simulation",
        nargs='*'
    )
    parser.add_argument(
        "--profile",
        help="Profile testbench execution time.",
        action="store_true"
    )
    parser.add_argument(
        "--preprocess",
        help="Toggle preprocess script",
        type=str2bool
    )
    parser.add_argument(
        "--postprocess",
        help="Toggle postprocess script",
        type=str2bool
    )
    # Collect cli args to kwargs dictionary
    cli_args = parser.parse_args()
    kwargs = vars(cli_args)

    # Create testbench and run it with given arguments
    tests = ACoreTests(**kwargs)
    tests.run(**kwargs)
