#!/usr/bin/env python3

"""
Print out the name of hardware config used by the given test config.
"""


import os
import sys
import yaml
import argparse
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--test-target', required=True)

    args = parser.parse_args()

    with open(args.test_target, 'r') as fd:
        config = yaml.load(fd, yaml.Loader)
        print(os.path.splitext(os.path.basename(config['hw_config_yaml']))[0])

if __name__=='__main__':
    main()
