#!/usr/bin/env python3

"""
Invoke make in the provided directory with the given hw configuration.
and map yaml hw configuration parameters to make flags.
"""

import os
import sys
if not (os.path.abspath('../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../thesdk'))
from thesdk import *
from ACoreTests.test_config import load_test_config_yaml
import acore_methods.pathutils as pathutils

import os
import yaml
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--test-config', required=True)
    parser.add_argument('--target-dir', required=True)
    parser.add_argument('--platform', required=True)
    parser.add_argument('--chisel-dir', required=True)
    args = parser.parse_args()

    test_config = load_test_config_yaml(args.test_config)
    test_config_hw = pathutils.barename(test_config['hw_config_yaml'])

    isa_string = test_config['hw_config']['isa_string']
    progmem_start = test_config['hw_config']['pc_init'].replace("h", "0x")
    progmem_length = str(2**int(test_config['hw_config']['progmem_depth']))
    datamem_start = "0x20000000"
    datamem_length = str(2**int(test_config['hw_config']['datamem_depth'])) 

    try:
        extra_args = test_config['make_args']
    except KeyError:
        extra_args = []
    
    compile_args = {
        'MARCH': isa_string,
        'MABI': 'ilp32f' if 'f' in isa_string else 'ilp32',
        'PLATFORM': args.platform,
        'A_CORE_LIB_PATH' : f'{args.target_dir}/a-core-library',
        'A_CORE_HEADERS_PATH' : f'{args.chisel_dir}/{test_config_hw}/include',
        'DEFAULT_MAKEFILE_PATH' : './a-core-library',
        'DEFAULT_LINKER_PATH' : './a-core-library',
        'PROGMEM_START' : progmem_start,
        'PROGMEM_LENGTH' : progmem_length,
        'DATAMEM_START' : datamem_start,
        'DATAMEM_LENGTH' : datamem_length
    }

    compile_args.update(extra_args)

    arg_str = ""
    for arg in compile_args:
        arg_str += f"{arg}={compile_args[arg]} "
    
    make_cmd = "make -C {} {}".format(args.target_dir, arg_str)
    os.system(make_cmd)

if __name__=='__main__':
    main()
