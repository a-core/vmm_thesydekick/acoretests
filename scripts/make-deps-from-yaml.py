#!/usr/bin/env python3

import os
import sys
import yaml
import argparse
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--test-target', required=True)

    args = parser.parse_args()


    with open(args.test_target, 'r') as fd:
        config = yaml.load(fd, yaml.Loader)

        hw_config_name = Path(config['hw_config_yaml']).stem
        print("build/chisel/%s" % hw_config_name, end=" ")
        
        test_target_name = os.path.splitext(os.path.basename(args.test_target))[0]
        print(f"build/tests/programs/{test_target_name}/{hw_config_name}/bin/$(platform).elf", end=" ")


if __name__=='__main__':
    main()
