#!/usr/bin/env python3


import os
import yaml
import argparse
import glob
import subprocess


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--riscv-test-case", required=True)
    parser.add_argument("--riscv-tests-path", required=True)
    parser.add_argument("--test-config", required=True)
    parser.add_argument("--platform", required=True)
    parser.add_argument("--extra-args", required=False, 
                        metavar="KEY=VALUE", nargs="+")

    args = parser.parse_args()

    tests = [
        f"rv32{args.riscv_test_case}-p-" +
        f"{os.path.splitext(os.path.basename(a))[0]}"
        for a in glob.glob(
            os.path.join(
                args.riscv_tests_path,
                "riscv-tests",
                "isa",
                f"rv32{args.riscv_test_case}",
                "*.S",
            )
        )
    ]

    results = []
    make_cmd = \
        ["make", "test_config=riscv-tests", f"platform={args.platform}"] \
        + args.extra_args
    cend = "\33[0m"
    cred = "\33[31m"
    cgreen = "\33[32m"
    if args.platform != 'sim':
        print()
        print("----------Info about FPGA runs:----------")
        print("The program starts a subprocess when FPGA is selected as platform.")
        print("The next test should not be started before the previous one finishes.")
        print("Observe the FPGA logs, as well as ASIC outputs.")
        print("This program will not print TheSyDeKick logs to reduce clutter, so")
        print("  use another test to ensure that the testbench is working fine.")
        print()
    for test in tests:
        with open(args.test_config, "r") as fd:
            config = yaml.load(fd, yaml.Loader)
        config["make_args"]["RISCV_TEST_NAME"] = test
        with open(args.test_config, "w") as fd:
            fd.write(yaml.dump(config))
        if args.platform == "sim":
            # Run and wait until process finishes
            code = os.system(' '.join(make_cmd))
            if code == 0:
                results.append([test, cgreen + "PASS" + cend])
            else:
                results.append([test, cred + "FAIL" + cend])
        else:
            # Start a process
            subprocess.Popen(make_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            print(f"Running test: {test}.")
            ans = input("Press q to quit, anything else to continue to the next test: ")
            if ans == 'q':
                break

    if args.platform == "sim":
        for entry in results:
            print("{:20s} {:5s}".format(entry[0], entry[1]))


if __name__ == "__main__":
    main()
