#!/usr/bin/env python3

import os
import sys
if not (os.path.abspath('../thesdk') in sys.path):
    sys.path.append(os.path.abspath('../thesdk'))
from thesdk import *
from chisel_methods import *
from ACoreChip import *

import os
import yaml
import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--chisel-path', required=True)
    parser.add_argument('--hw-config', required=True)
    parser.add_argument('--target-dir', required=True)
    args = parser.parse_args()

    with open(args.hw_config, 'r') as fd:
        config = yaml.load(fd, yaml.Loader)
        chisel = chisel_methods()
        chisel.run_chisel_generator(
            entity=ACoreChip(),
            mainclass='acorechip.ACoreChip',
            args={
                '-core_config': args.hw_config,
                '-jtag_config': config['jtag_config_yaml'],
                '-cheaders_target_dir': os.path.join(args.target_dir, 'include'),
                '-gen_cheaders': 'true',
                '--target-dir': args.target_dir
            }
        )

if __name__=='__main__':
    main()
