#!/usr/bin/env python3

"""
Print out the test program source directory of the given test configuration.
"""

import os
import sys
import yaml
import argparse
from pathlib import Path


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--test-config', required=True)
    parser.add_argument('--field', required=True)

    args = parser.parse_args()

    with open(args.test_config, 'r') as fd:
        config = yaml.load(fd, yaml.Loader)
        print(config[args.field])

if __name__=='__main__':
    main()