#!/usr/bin/env bash
#Selective initialization of submodules
#Written by by Marko Kosunen, marko.kosunen@aalto.fi, 2017
DIR=$( cd `dirname $0` && pwd )
SUBMODULES="\
    ./sw/riscv-assembly-tests \
    ./sw/riscv-c-tests \
    ./sw/riscv-coremark-tests \
    ./sw/riscv-tests \
    ./sw/a-core-library \
    ./sw/mnist-tests \
    ./sw/vmm-tests \
"

git submodule sync
for mod in $SUBMODULES; do 
    git submodule update --init $mod
    cd ${mod}
    if [ -f ./init_submodules.sh ]; then
        ./init_submodules.sh
    fi
    cd ${DIR}

done
exit 0

