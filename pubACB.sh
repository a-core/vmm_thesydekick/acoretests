if ! cd ../ACoreChip/chisel/ACoreBase ; then
    echo "You should be under Entities/ACoreTests when running this script"
    exit 1
fi
echo "Publishing ACoreBase"
sbt publishLocal
cd ../
echo "Publishing ACoreChip"
sbt publishLocal
cd ../../ACoreTests
echo "Finished publishing"
